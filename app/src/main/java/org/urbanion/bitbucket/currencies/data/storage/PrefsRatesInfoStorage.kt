package org.urbanion.bitbucket.currencies.data.storage

import android.content.SharedPreferences
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Maybe
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo

/**
 *
 * @author jagger on 10.12.18.
 */
class PrefsRatesInfoStorage(
    private val prefs: SharedPreferences,
    private val gson: Gson
) : RatesInfoStorage {
    override fun write(rates: RatesInfo): Completable =
        Completable
            .fromCallable {
                val jsonString = gson.toJson(rates)
                prefs.edit().putString(RATES, jsonString).apply()
            }

    override fun read(): Maybe<RatesInfo> =
        Maybe
            .fromCallable {
                val jsonString = prefs.getString(RATES, "")
                if (jsonString.isNotEmpty()) {
                    gson.fromJson(jsonString, RatesInfo::class.java)
                } else {
                    null
                }
            }

    companion object {
        private const val RATES = "RATES"
    }
}