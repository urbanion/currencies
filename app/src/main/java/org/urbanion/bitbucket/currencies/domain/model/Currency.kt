package org.urbanion.bitbucket.currencies.domain.model

import android.support.annotation.DrawableRes
import org.urbanion.bitbucket.currencies.R

/**
 *
 * @author jagger on 25.11.18.
 *
 * There is an assumption that the app knows all currencies returned from the backend.
 */
enum class Currency {
    EUR,
    GBP,
    USD,
    JPY,
    AUD,
    BGN,
    BRL,
    RUB,
    CAD,
    CHF,
    CNY,
    CZK,
    DKK,
    HKD,
    HRK,
    HUF,
    IDR,
    ILS,
    INR,
    PHP,
    PLN
}

