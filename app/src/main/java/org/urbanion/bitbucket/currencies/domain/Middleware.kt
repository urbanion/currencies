package org.urbanion.bitbucket.currencies.domain

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.zipWith
import io.reactivex.subjects.PublishSubject
import org.urbanion.bitbucket.currencies.domain.model.Action
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo
import java.util.concurrent.TimeUnit

/**
 *
 * @author jagger on 11.12.18.
 */
class Middleware(
    private val ratesRepo: RatesRepository,
    private val connectivityRepo: ConnectivityRepository,
    private val ioScheduler: Scheduler,
    private val computationScheduler: Scheduler
) {
    private val sideEffects = PublishSubject.create<Action>()

    fun process(userActions: Observable<Action>): Observable<Action> =
        Observable
            .merge(
                observeRates(userActions),
                userActions,
                sideEffects
            )

    private fun observeRates(userActions: Observable<Action>): Observable<Action> =
        Observable
            .merge(
                ratesRepo.getLastLoadedRates().toObservable(),
                observeNewRates(observeRetry(userActions))
            )
            .map<Action> { Action.RatesInfoChanged(it) }

    private fun observeNewRates(retry: Observable<Any>): Observable<RatesInfo> =
        ratesRepo
            .getRates()
            .toFlowable()
            .repeat()
            .subscribeOn(ioScheduler)
            .observeOn(computationScheduler, false, RATES_BUFFER_SIZE)
            .zipWith(Flowable.interval(0, RATES_DELAY_SECONDS, TimeUnit.SECONDS)) { rates, _ -> rates }
            .toObservable()
            .retry(1) // Retry once in case network appears before request finishes
            .doOnError { sideEffects.onNext(Action.LoadRatesError) }
            .retryWhen { errors ->
                errors.flatMap { retry }
            }

    private fun observeRetry(userActions: Observable<Action>): Observable<Any> =
        Observable
            .merge(
                userActions.filter { it is Action.Retry },
                connectivityRepo.observeNetworkConnected()
            )

    companion object {
        private const val RATES_BUFFER_SIZE = 3
        private const val RATES_DELAY_SECONDS = 1L
    }
}