package org.urbanion.bitbucket.currencies.presentation.util

import android.content.Context

/**
 *
 * @author jagger on 08.12.18.
 */
class IdentifierProviderImpl(private val context: Context) : IdentifierProvider {
    override fun getStringIdentifier(name: String): Int = getIdentifier(name, "string")

    override fun getDrawableIdentifier(name: String): Int = getIdentifier(name, "drawable")

    private fun getIdentifier(name: String, type: String): Int =
        context.resources.getIdentifier(name, type, "org.urbanion.bitbucket.currencies")
}