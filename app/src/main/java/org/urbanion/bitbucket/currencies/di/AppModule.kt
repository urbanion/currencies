package org.urbanion.bitbucket.currencies.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import org.urbanion.bitbucket.currencies.data.ConnectivityRepositoryImpl
import org.urbanion.bitbucket.currencies.data.Api
import org.urbanion.bitbucket.currencies.domain.ConnectivityRepository
import org.urbanion.bitbucket.currencies.presentation.util.IdentifierProvider
import org.urbanion.bitbucket.currencies.presentation.util.IdentifierProviderImpl
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 *
 * @author jagger on 08.12.18.
 */
fun createAppModule(context: Context) = module {
    single<OkHttpClient> {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        OkHttpClient.Builder()
            // Workaround for SocketTimeoutException from HTTP/2 connection leaves dead okhttp clients in pool
            // https://github.com/square/okhttp/issues/3146
            .protocols(listOf(Protocol.HTTP_1_1))
            .addInterceptor(loggingInterceptor)
            .build()
    }
    single {
        Gson()
    }
    single<Api> {
        Retrofit.Builder()
            .client(get())
            .baseUrl("https://revolut.duckdns.org")
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(Api::class.java)
    }
    single<SharedPreferences> {
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)
    }
    single<ConnectivityRepository> {
        ConnectivityRepositoryImpl(context)
    }
    single<IdentifierProvider> {
        IdentifierProviderImpl(context)
    }
}

