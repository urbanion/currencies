package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo

/**
 *
 * @author jagger on 10.12.18.
 */
fun recalcItems(source: Money, items: List<Money>, ratesInfo: RatesInfo): List<Money> =
    items.map {
        Money(
            currency = it.currency,
            amount = convert(
                from = source.currency,
                to = it.currency,
                amount = source.amount,
                ratesInfo = ratesInfo
            )
        )
    }

fun convert(from: Currency, to: Currency, amount: Double, ratesInfo: RatesInfo): Double =
    when {
        from == to -> amount
        from == ratesInfo.base -> amount * ratesInfo.rates[to]!!
        to == ratesInfo.base -> amount / ratesInfo.rates[from]!!
        else -> (amount / ratesInfo.rates[from]!!) * ratesInfo.rates[to]!!
    }