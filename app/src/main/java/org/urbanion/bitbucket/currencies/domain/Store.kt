package org.urbanion.bitbucket.currencies.domain

import io.reactivex.Observable
import org.urbanion.bitbucket.currencies.domain.model.*
import org.urbanion.bitbucket.currencies.domain.reducers.*

/**
 *
 * @author jagger on 27.11.18.
 */
class Store(private val middleware: Middleware) {
    fun observeState(userActions: Observable<Action>): Observable<State> =
        middleware.process(userActions).scan<State>(State.Loading) { state, action -> reduce(state, action) }

    private fun reduce(state: State, action: Action): State =
        when (action) {
            is Action.RatesInfoChanged -> reduceRates(state, action.ratesInfo)
            is Action.RaiseOn -> reduceRaiseOn(state, action.currency)
            is Action.AmountChanged -> reduceAmountChanged(state, action.money)
            is Action.LoadRatesError -> reduceLoadRatesError(state)
            is Action.Retry -> reduceRetry(state)
        }
}