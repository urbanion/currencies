package org.urbanion.bitbucket.currencies.di

import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.urbanion.bitbucket.currencies.data.RatesRepositoryImpl
import org.urbanion.bitbucket.currencies.data.storage.PrefsRatesInfoStorage
import org.urbanion.bitbucket.currencies.data.storage.RatesInfoStorage
import org.urbanion.bitbucket.currencies.domain.Middleware
import org.urbanion.bitbucket.currencies.domain.RatesRepository
import org.urbanion.bitbucket.currencies.domain.Store
import org.urbanion.bitbucket.currencies.presentation.MainViewModel

/**
 *
 * @author jagger on 10.12.18.
 */
fun createRatesModule() = module {
    single<RatesInfoStorage> {
        PrefsRatesInfoStorage(
            prefs = get(),
            gson = get()
        )
    }
    single<RatesRepository> {
        RatesRepositoryImpl(
            api = get(),
            storage = get()
        )
    }
    factory {
        Middleware(
            ratesRepo = get(),
            connectivityRepo = get(),
            ioScheduler = Schedulers.io(),
            computationScheduler = Schedulers.computation()
        )
    }
    factory {
        Store(
            middleware = get()
        )
    }
    viewModel {
        MainViewModel(
            store = get(),
            identifierProvider = get()
        )
    }
}