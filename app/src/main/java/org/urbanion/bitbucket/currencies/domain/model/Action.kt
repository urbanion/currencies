package org.urbanion.bitbucket.currencies.domain.model

/**
 *
 * @author jagger on 26.11.18.
 */
sealed class Action {
    class AmountChanged(val money: Money) : Action()
    class RatesInfoChanged(val ratesInfo: RatesInfo) : Action()
    object LoadRatesError: Action()
    class RaiseOn(val currency: Currency) : Action()
    object Retry : Action()
}
