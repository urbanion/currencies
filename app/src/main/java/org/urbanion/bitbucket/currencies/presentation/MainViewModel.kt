package org.urbanion.bitbucket.currencies.presentation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.urbanion.bitbucket.currencies.R
import org.urbanion.bitbucket.currencies.domain.Store
import org.urbanion.bitbucket.currencies.domain.model.Action
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.State
import org.urbanion.bitbucket.currencies.presentation.model.UIMoney
import org.urbanion.bitbucket.currencies.presentation.model.UIState
import org.urbanion.bitbucket.currencies.presentation.util.IdentifierProvider
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * @author jagger on 25.11.18.
 */
class MainViewModel(
    store: Store,
    private val identifierProvider: IdentifierProvider
) : ViewModel() {
    private val state = MutableLiveData<UIState>()
    private val userActions = PublishSubject.create<Action>()
    private val stateDisposable: Disposable
    private val dateFormat = SimpleDateFormat("d MMM yyyy HH:mm:ss:SSS", Locale.ENGLISH)

    init {
        stateDisposable = store
            .observeState(userActions)
            .map { convertState(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d("State", "State updated to ${it.javaClass.simpleName}")
                state.value = it
            }
    }

    override fun onCleared() {
        super.onCleared()
        stateDisposable.dispose()
    }

    fun getState(): LiveData<UIState> = state

    fun onItemClicked(currency: Currency) = userActions.onNext(Action.RaiseOn(currency))

    fun onAmountChanged(money: Money) = userActions.onNext(Action.AmountChanged(money))

    fun onRetryClicked() = userActions.onNext(Action.Retry)

    private fun convertState(state: State): UIState =
        when (state) {
            is State.Loading -> UIState.Loading
            is State.Success -> UIState.Success(
                items = state.items.map { convertMoney(it) },
                subtitle = dateFormat.format(state.ratesInfo.date),
                subtitleColor = if (state.updatesStopped) R.color.toolbarInactive else R.color.toolbarActive
            )
            is State.Error -> UIState.Error
        }

    private fun convertMoney(money: Money): UIMoney {
        val currencyCode = money.currency.name.toLowerCase()
        return UIMoney(
            currency = money.currency,
            amount = money.amount,
            currencyName = identifierProvider.getStringIdentifier("name_$currencyCode"),
            currencyFlag = identifierProvider.getDrawableIdentifier("ic_$currencyCode")
        )
    }
}