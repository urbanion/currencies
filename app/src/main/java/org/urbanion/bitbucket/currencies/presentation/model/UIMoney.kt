package org.urbanion.bitbucket.currencies.presentation.model

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import org.urbanion.bitbucket.currencies.domain.model.Currency

/**
 *
 * @author jagger on 08.12.18.
 */
data class UIMoney(
    val currency: Currency,
    val amount: Double,
    @StringRes val currencyName: Int,
    @DrawableRes val currencyFlag: Int
)