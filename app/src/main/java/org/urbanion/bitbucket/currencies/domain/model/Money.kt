package org.urbanion.bitbucket.currencies.domain.model

/**
 *
 * @author jagger on 25.11.18.
 */
data class Money(
    val currency: Currency,
    val amount: Double
)