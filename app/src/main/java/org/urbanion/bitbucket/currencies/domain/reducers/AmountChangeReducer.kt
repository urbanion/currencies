package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 10.12.18.
 */
fun reduceAmountChanged(state: State, source: Money): State =
    when (state) {
        is State.Success -> state.copy(items = recalcItems(source, state.items, state.ratesInfo))
        else -> throw IllegalArgumentException("Amount can't be changed in state other than Success")
    }