package org.urbanion.bitbucket.currencies.presentation

import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.AbsListView
import kotlinx.android.synthetic.main.activity_main.*
import android.view.inputmethod.InputMethodManager
import org.koin.android.viewmodel.ext.android.viewModel
import org.urbanion.bitbucket.currencies.R
import org.urbanion.bitbucket.currencies.presentation.model.UIMoney
import org.urbanion.bitbucket.currencies.presentation.model.UIState


class MainActivity : AppCompatActivity() {
    private val itemAnimator = DefaultItemAnimator()
    private var currenciesOrderFootprint: String? = null
    private var currentView: View? = null
    private lateinit var adapter: MoneyAdapter
    private val model: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initList()
        btnRetry.setOnClickListener { model.onRetryClicked() }
        model.getState().observe(this, Observer<UIState> { render(it!!) })
    }

    private fun initList() {
        adapter = MoneyAdapter(
            onItemClicked = model::onItemClicked,
            onAmountChanged = model::onAmountChanged
        )
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter
        list.onScrolled {
            // Hide keyboard
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(list.windowToken, 0)
        }
    }

    private fun render(state: UIState) =
        when (state) {
            is UIState.Loading -> changeView(progress)
            is UIState.Error -> changeView(error)
            is UIState.Success -> {
                changeView(list)
                updateItems(state.items)
                updateToolbar(state.subtitle, state.subtitleColor)
            }
        }

    private fun updateItems(items: List<UIMoney>) {
        val orderFootprint = items.asSequence().map { it.currency }.joinToString()
        // Animate only item position change
        list.itemAnimator = if (orderFootprint == currenciesOrderFootprint) null else itemAnimator
        adapter.submitList(items)
        currenciesOrderFootprint = orderFootprint
    }

    private fun updateToolbar(subtitleText: String, @ColorRes subtitleColor: Int) {
        toolbar.apply {
            subtitle = subtitleText
            val color = ContextCompat.getColor(context, subtitleColor)
            setSubtitleTextColor(color)
        }
    }

    private fun changeView(view: View) {
        if (currentView !== view) {
            currentView?.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out))
            view.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in))
            currentView?.visibility = View.GONE
            view.visibility = View.VISIBLE
            currentView = view
        }
    }

    private fun RecyclerView.onScrolled(block: () -> Unit) {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING ||
                        newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    block.invoke()
                }
            }
        })
    }
}
