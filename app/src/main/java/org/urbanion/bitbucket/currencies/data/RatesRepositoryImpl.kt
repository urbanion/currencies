package org.urbanion.bitbucket.currencies.data

import io.reactivex.Maybe
import io.reactivex.Single
import org.urbanion.bitbucket.currencies.data.model.NWRatesInfo
import org.urbanion.bitbucket.currencies.data.storage.RatesInfoStorage
import org.urbanion.bitbucket.currencies.domain.RatesRepository
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo
import java.lang.IllegalArgumentException
import java.util.*

/**
 *
 * @author jagger on 25.11.18.
 */
class RatesRepositoryImpl(private val api: Api, private val storage: RatesInfoStorage) : RatesRepository {

    override fun getRates(): Single<RatesInfo> =
            api
                .getRates(Currency.EUR)
                .map { convertRatesInfo(it) }
                .flatMap {  rates ->
                    storage
                        .write(rates)
                        .onErrorComplete()
                        .andThen(Single.just(rates))
                }

    override fun getLastLoadedRates(): Maybe<RatesInfo> = storage.read()

    private fun convertRatesInfo(src: NWRatesInfo) = RatesInfo(
        base = requireNotNull(src.base?.toCurrency()) { "base must not be null" },
        rates = convertRates(
            src = requireNotNull(src.rates) { "rates must not be null" }
        ),
        date = Date()
    )

    private fun convertRates(src: Map<String, Double>): Map<Currency, Double> =
        src.mapNotNull { (currencyCode, rate) ->
            if (rate > 0) {
                currencyCode.toCurrency()?.let { it to rate }
            } else {
                null
            }
        }.toMap()

    private fun String.toCurrency(): Currency? =
        try {
            Currency.valueOf(this)
        } catch (e: IllegalArgumentException) {
            null
        }
}