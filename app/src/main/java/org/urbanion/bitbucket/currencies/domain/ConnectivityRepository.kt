package org.urbanion.bitbucket.currencies.domain

import io.reactivex.Observable

/**
 *
 * @author jagger on 09.12.18.
 */
interface ConnectivityRepository {
    fun observeNetworkConnected(): Observable<Unit>
}