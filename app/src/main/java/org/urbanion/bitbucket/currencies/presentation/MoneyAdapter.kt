package org.urbanion.bitbucket.currencies.presentation

import android.content.Context
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_money.*
import org.urbanion.bitbucket.currencies.R
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.presentation.model.UIMoney
import java.util.*

/**
 *
 * @author jagger on 25.11.18.
 */
private val moneyDiffCallback = object : DiffUtil.ItemCallback<UIMoney>() {
    override fun areItemsTheSame(first: UIMoney, second: UIMoney): Boolean =
        first.currency == second.currency

    override fun areContentsTheSame(first: UIMoney, second: UIMoney): Boolean =
        first.amount == second.amount
}

class MoneyAdapter(
    private val onItemClicked: (Currency) -> Unit,
    private val onAmountChanged: (Money) -> Unit
) : ListAdapter<UIMoney, MoneyViewHolder>(moneyDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoneyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_money, parent, false)
        return MoneyViewHolder(view, onItemClicked, onAmountChanged)
    }

    override fun onBindViewHolder(viewHolder: MoneyViewHolder, position: Int) {
        val money = getItem(position)
        viewHolder.bind(money)
    }
}

class MoneyViewHolder(
    override val containerView: View,
    private val onItemClicked: (Currency) -> Unit,
    private val onAmountChanged: (Money) -> Unit
) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var currentMoney: UIMoney? = null

    init {
        initAmountChangeListener()
        initItemClickListener()
        initKeyboardDoneListener()
    }

    fun bind(money: UIMoney) {
        currentMoney = money
        ivFlag.setImageResource(money.currencyFlag)
        tvCurrencyCode.text = money.currency.name
        tvCurrencyFullName.setText(money.currencyName)
        if (!etAmount.isFocused) {
            etAmount.setText("%.2f".format(Locale.ENGLISH, money.amount), TextView.BufferType.NORMAL)
        }
    }

    private fun initAmountChangeListener() {
        etAmount.addTextChangeListener { str ->
            currentMoney?.let { money ->
                val amount = if (str.isEmpty()) 0.0 else str.toDouble()
                onAmountChanged(Money(money.currency, amount))
            }
        }
    }

    private fun initItemClickListener() {
        itemView.setOnClickListener {
            currentMoney?.let {  money ->
                etAmount.requestFocus()
                etAmount.setSelection(etAmount.text.length)
                // Show keyboard
                val imm = containerView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(etAmount, InputMethodManager.SHOW_FORCED)
                onItemClicked(money.currency)
            }
        }

        etAmount.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                currentMoney?.let { money ->
                    onItemClicked(money.currency)
                }
            }
            false
        }
    }

    private fun initKeyboardDoneListener() {
        etAmount.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                etAmount.post {
                    etAmount.clearFocus()
                }
            }
            false
        }
    }
}

private fun EditText.addTextChangeListener(listener: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            if (isFocused) {
                listener.invoke(s.toString())
            }
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        }
    })
}