package org.urbanion.bitbucket.currencies.domain

import io.reactivex.Maybe
import io.reactivex.Single
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo

/**
 *
 * @author jagger on 25.11.18.
 */
interface RatesRepository {
    fun getRates(): Single<RatesInfo>
    fun getLastLoadedRates(): Maybe<RatesInfo>
}