package org.urbanion.bitbucket.currencies.data

import io.reactivex.Single
import org.urbanion.bitbucket.currencies.data.model.NWRatesInfo
import org.urbanion.bitbucket.currencies.domain.model.Currency
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * @author jagger on 08.12.18.
 */
interface Api {
    @GET("latest")
    fun getRates(@Query("base") base: Currency): Single<NWRatesInfo>
}