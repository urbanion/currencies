package org.urbanion.bitbucket.currencies.data.storage

import io.reactivex.Completable
import io.reactivex.Maybe
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo

/**
 *
 * @author jagger on 10.12.18.
 */
interface RatesInfoStorage {
    fun write(rates: RatesInfo): Completable
    fun read(): Maybe<RatesInfo>
}