package org.urbanion.bitbucket.currencies.presentation.util

/**
 *
 * @author jagger on 08.12.18.
 */
interface IdentifierProvider {
    fun getStringIdentifier(name: String): Int
    fun getDrawableIdentifier(name: String): Int
}