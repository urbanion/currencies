package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 10.12.18.
 */
private const val BASE_AMOUNT = 100.0

fun reduceRates(state: State, ratesInfo: RatesInfo): State =
    when (state) {
        is State.Success -> {
            val source = state.items[0]
            State.Success(
                items = recalcItems(source, state.items, ratesInfo),
                ratesInfo = ratesInfo,
                updatesStopped = false
            )
        }
        else -> State.Success(
            items = createItems(ratesInfo),
            ratesInfo = ratesInfo,
            updatesStopped = false
        )
    }

private fun createItems(ratesInfo: RatesInfo): List<Money> {
    val base = Money(ratesInfo.base, BASE_AMOUNT)
    val other = ratesInfo.rates.entries.map { (currency, rate) ->
        Money(
            currency = currency,
            amount = base.amount * rate
        )
    }
    return listOf(base) + other
}