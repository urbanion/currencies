package org.urbanion.bitbucket.currencies.domain.model

/**
 *
 * @author jagger on 25.11.18.
 */
sealed class State {
    object Loading : State()
    data class Success(
        val items: List<Money>,
        val ratesInfo: RatesInfo,
        val updatesStopped: Boolean
    ) : State()
    object Error : State()
}