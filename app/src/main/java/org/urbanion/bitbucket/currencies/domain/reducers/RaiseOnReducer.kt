package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 10.12.18.
 */
fun reduceRaiseOn(state: State, currency: Currency): State =
    if (state is State.Success) {
        val items = state.items.toMutableList()
        val target = items.find { it.currency == currency }!!
        val index = items.indexOf(target)
        items.removeAt(index)
        items.add(0, target)
        state.copy(items = items)
    } else {
        throw IllegalArgumentException("Item can't be raised on in state other than Success")
    }