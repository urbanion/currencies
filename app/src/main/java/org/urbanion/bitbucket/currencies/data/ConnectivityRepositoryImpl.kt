package org.urbanion.bitbucket.currencies.data

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.urbanion.bitbucket.currencies.domain.ConnectivityRepository
import java.util.concurrent.atomic.AtomicBoolean

/**
 *
 * @author jagger on 09.12.18.
 */
class ConnectivityRepositoryImpl(private val context: Context) : ConnectivityRepository {
    private val isConnected = PublishSubject.create<Unit>()
    private val isInitialized = AtomicBoolean()

    override fun observeNetworkConnected(): Observable<Unit> {
        if (!isInitialized.getAndSet(true)) {
            initConnectivityListener()
        }
        return isConnected
    }

    private fun initConnectivityListener() {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            initConnectivityListener24(connectivityManager)
        } else {
            initConnectivityListenerPre24(connectivityManager)
        }
    }

    @TargetApi(24)
    private fun initConnectivityListener24(connectivityManager: ConnectivityManager) {
        connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                super.onAvailable(network)
                isConnected.onNext(Unit)
            }
        })
    }

    private fun initConnectivityListenerPre24(connectivityManager: ConnectivityManager) {
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (connectivityManager.activeNetworkInfo?.isConnected == true) {
                    isConnected.onNext(Unit)
                }
            }
        }, intentFilter)
    }
}