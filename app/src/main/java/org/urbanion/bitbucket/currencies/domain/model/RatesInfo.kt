package org.urbanion.bitbucket.currencies.domain.model

import java.util.*

/**
 *
 * @author jagger on 25.11.18.
 */
data class RatesInfo(
    val base: Currency,
    val rates: Map<Currency, Double>,
    val date: Date
)