package org.urbanion.bitbucket.currencies.data.model

/**
 *
 * @author jagger on 08.12.18.
 */
class NWRatesInfo(
    val base: String?,
    val rates: Map<String, Double>?
)