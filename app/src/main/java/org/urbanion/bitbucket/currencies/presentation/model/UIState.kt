package org.urbanion.bitbucket.currencies.presentation.model

import android.support.annotation.ColorRes

/**
 *
 * @author jagger on 08.12.18.
 */
sealed class UIState {
    object Loading : UIState()
    data class Success(
        val items: List<UIMoney>,
        val subtitle: String,
        @ColorRes val subtitleColor: Int) : UIState()
    object Error : UIState()
}