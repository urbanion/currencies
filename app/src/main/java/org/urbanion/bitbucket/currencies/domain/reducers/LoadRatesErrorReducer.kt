package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 10.12.18.
 */
fun reduceLoadRatesError(state: State): State =
    when (state) {
        is State.Success -> state.copy(updatesStopped = true)
        else -> State.Error
    }