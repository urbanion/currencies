package org.urbanion.bitbucket.currencies

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.urbanion.bitbucket.currencies.di.createAppModule
import org.urbanion.bitbucket.currencies.di.createRatesModule

/**
 *
 * @author jagger on 08.12.18.
 */
class CurrenciesApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(createAppModule(this), createRatesModule()))
    }
}