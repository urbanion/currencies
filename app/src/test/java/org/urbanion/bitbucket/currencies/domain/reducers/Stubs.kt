package org.urbanion.bitbucket.currencies.domain.reducers

import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo
import org.urbanion.bitbucket.currencies.domain.model.State
import java.util.*

/**
 *
 * @author jagger on 14.12.18.
 */
val eurRates = RatesInfo(
    base = Currency.EUR,
    rates = mapOf(
        Currency.CAD to 1.5369,
        Currency.RUB to 79.735
    ),
    date = Date()
)

val state = State.Success(
    items = listOf(
        Money(Currency.EUR, 100.0),
        Money(Currency.CAD, 153.69),
        Money(Currency.RUB, 7973.5)
    ),
    ratesInfo = eurRates,
    updatesStopped = false
)