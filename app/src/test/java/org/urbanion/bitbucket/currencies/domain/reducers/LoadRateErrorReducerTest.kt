package org.urbanion.bitbucket.currencies.domain.reducers

import org.junit.Assert
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 14.12.18.
 */
class LoadRateErrorReducerTest : Assert() {
    @Test
    fun `loading state should be updated to error state when error occur`() {
        val newState = reduceLoadRatesError(State.Loading)
        assertEquals(State.Error, newState)
    }

    @Test
    fun `error state should not change when error occur`() {
        val newState = reduceLoadRatesError(State.Error)
        assertEquals(State.Error, newState)
    }

    @Test
    fun `success state should indacate when error occur`() {
        val newState = reduceLoadRatesError(state)
        val expected = state.copy(updatesStopped = true)
        assertEquals(expected, newState)
    }
}