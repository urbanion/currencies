package org.urbanion.bitbucket.currencies.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo
import org.urbanion.bitbucket.currencies.domain.model.Action
import org.urbanion.bitbucket.currencies.domain.model.Currency
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeUnit


/**
 *
 * @author jagger on 11.12.18.
 */
class MiddlewareTest : Assert() {
    private val ratesRepo = mock<RatesRepository>()
    private val connectivityRepo = mock<ConnectivityRepository>()
    private val stubRatesInfo = RatesInfo(Currency.EUR, emptyMap(), Date())
    private lateinit var scheduler: TestScheduler
    private lateinit var middleware: Middleware
    private lateinit var observer: TestObserver<Action>

    @Before
    fun before() {
        scheduler = TestScheduler()
        middleware = Middleware(ratesRepo, connectivityRepo, scheduler, scheduler)
        observer = TestObserver()
    }

    @Test
    fun `emit error action when load rates error occur`() {
        whenever(connectivityRepo.observeNetworkConnected()).thenReturn(Observable.never())
        whenever(ratesRepo.getLastLoadedRates()).thenReturn(Maybe.empty())
        whenever(ratesRepo.getRates()).thenReturn(Single.error(UnknownHostException()))
        middleware.process(Observable.never()).subscribe(observer)
        scheduler.advanceTimeBy(2000, TimeUnit.MILLISECONDS)
        assertEquals(1, observer.valueCount())
        assertEquals(Action.LoadRatesError, observer.values().first())
    }

    @Test
    fun `emit cached value first`() {
        whenever(connectivityRepo.observeNetworkConnected()).thenReturn(Observable.never())
        whenever(ratesRepo.getLastLoadedRates()).thenReturn(Maybe.just(stubRatesInfo))
        whenever(ratesRepo.getRates()).thenReturn(Single.error(UnknownHostException()))
        middleware.process(Observable.never()).subscribe(observer)
        scheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        assertEquals(2, observer.valueCount())
        assertTrue(observer.values()[0] is Action.RatesInfoChanged)
        assertTrue(observer.values()[1] is Action.LoadRatesError)
    }
}