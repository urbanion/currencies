package org.urbanion.bitbucket.currencies.domain.reducers

import org.junit.Assert
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 14.12.18.
 */
class AmountChangeReducerTest : Assert() {
    @Test(expected = IllegalArgumentException::class)
    fun `amount change in loading state should fire exception`() {
        reduceAmountChanged(State.Loading, Money(Currency.EUR, 200.0))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `amount change in error state should fire exception`() {
        reduceAmountChanged(State.Error, Money(Currency.EUR, 200.0))
    }

    @Test
    fun `amount change in first item should update other items`() {
        val newState = reduceAmountChanged(state, Money(Currency.EUR, 10.0))
        val updatedAmount = (newState as State.Success).items[1].amount
        assertEquals(15.369, updatedAmount, 0.001)
    }
}