package org.urbanion.bitbucket.currencies.domain.reducers

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.RatesInfo

/**
 *
 * @author jagger on 14.12.18.
 */
@RunWith(Parameterized::class)
class UtilTest(
    private val from: Currency,
    private val to: Currency,
    private val amount: Double,
    private val ratesInfo: RatesInfo,
    private val expected: Double
) {
    @Test
    fun `convert`() {
        Assert.assertEquals(expected, convert(from, to, amount, ratesInfo), 0.001)
    }

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<Array<Any>> {
            return listOf(
                arrayOf(Currency.EUR, Currency.EUR, 6, eurRates, 6),
                arrayOf(Currency.EUR, Currency.RUB, 0, eurRates, 0),
                arrayOf(Currency.EUR, Currency.RUB, 1, eurRates, 79.735),
                arrayOf(Currency.EUR, Currency.RUB, 12.37, eurRates, 986.321),
                arrayOf(Currency.EUR, Currency.RUB, 0.025, eurRates, 1.993),
                arrayOf(Currency.RUB, Currency.EUR, 0, eurRates, 0),
                arrayOf(Currency.RUB, Currency.EUR, 1, eurRates, 0.012),
                arrayOf(Currency.RUB, Currency.EUR, 12.37, eurRates, 0.155),
                arrayOf(Currency.RUB, Currency.EUR, 0.64, eurRates, 0.008),
                arrayOf(Currency.RUB, Currency.CAD, 0, eurRates, 0),
                arrayOf(Currency.RUB, Currency.CAD, 1, eurRates, 0.019),
                arrayOf(Currency.RUB, Currency.CAD, 12.37, eurRates, 0.238),
                arrayOf(Currency.RUB, Currency.CAD, 0.64, eurRates, 0.012)
            )
        }
    }
}