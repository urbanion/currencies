package org.urbanion.bitbucket.currencies.domain.reducers

import org.junit.Assert
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 14.12.18.
 */
class RetryReducerTest : Assert() {
    @Test
    fun `error state should be changed to loading state when retry occur`() {
        val newState = reduceRetry(State.Error)
        assertEquals(State.Loading, newState)
    }

    @Test
    fun `success state should not be changed when retry occur`() {
        val newState = reduceRetry(state)
        assertEquals(state, newState)
    }
}