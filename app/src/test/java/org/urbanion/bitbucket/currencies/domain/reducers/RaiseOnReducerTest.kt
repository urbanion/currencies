package org.urbanion.bitbucket.currencies.domain.reducers

import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.Currency
import org.urbanion.bitbucket.currencies.domain.model.Money
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 14.12.18.
 */
class RaiseOnReducerTest : Assert() {
    @Test(expected = IllegalArgumentException::class)
    fun `currency raise on in loading state should fire exception`() {
        reduceRaiseOn(State.Loading, Currency.RUB)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `currency raise on in error state should fire exception`() {
        reduceRaiseOn(State.Error, Currency.RUB)
    }

    @Test
    fun `when item is raised on it should appear at first position`() {
        val newState = reduceRaiseOn(state, Currency.RUB)
        val firstItem = (newState as State.Success).items[0].currency
        assertEquals(Currency.RUB, firstItem)
    }

    @Test
    fun `when first item is raised on it should not change its position`() {
        val newState = reduceRaiseOn(state, Currency.RUB)
        val firstItem = (newState as State.Success).items[0]
        assertEquals(newState.items[0], firstItem)
    }
}