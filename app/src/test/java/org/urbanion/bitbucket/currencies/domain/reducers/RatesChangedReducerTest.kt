package org.urbanion.bitbucket.currencies.domain.reducers

import org.junit.Assert
import org.junit.Test
import org.urbanion.bitbucket.currencies.domain.model.State

/**
 *
 * @author jagger on 14.12.18.
 */
class RatesChangedReducerTest : Assert() {
    @Test
    fun `loading state should be changed to success state when rates updated`() {
        val newState = reduceRates(State.Loading, eurRates)
        assertEquals(state, newState)
    }

    @Test
    fun `amounts should be recalculated when rates updated`() {
        val multiply = 2
        val newRates = eurRates.copy(rates = eurRates.rates.map { it.key to it.value * multiply }.toMap())
        val newState = reduceRates(state, newRates) as State.Success
        for (i in 1 until state.items.size) {
            assertEquals(state.items[i].amount * multiply, newState.items[i].amount, 0.001)
        }
    }
}